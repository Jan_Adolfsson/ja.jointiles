﻿using System;

namespace Ja.JoinTiles
{
	class Output
	{
		internal static void BlankLine()
		{
			Console.WriteLine();
		}
		internal static void Msg(string msg)
		{
			Console.WriteLine(msg);
		}
		internal static void Error(string msg)
		{
			Console.WriteLine();
			var txtColor = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(msg);
			Console.ForegroundColor = txtColor;
		}
		internal static void Usage()
		{
			BlankLine();
			Msg("Join Tiles");
			Msg("============");
			Msg("Join multiple tilesheets.");
			Msg("");
			Msg("JILES destination [-f file0, ... ,fileX | -d directory]");
			Msg("");
			Msg("  destination			Specifies path to new joined sheet.");
			Msg("  -f					Specify source sheets by filepaths..");
			Msg("  file0, ... ,fileX	Paths to files to join.");
			Msg("  -m					Specify source sheets by directory.");
			Msg("  directory			Path to directory containing sheets.");

			BlankLine();
		}
	}
}
