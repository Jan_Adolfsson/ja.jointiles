﻿using System;

namespace Ja.JoinTiles
{
	class Program
	{
		static void Main(string[] argArray)
		{
			try
			{
				Output.Msg($"Parse arguments.");
				var args = Args.Parse(argArray);

				Output.Msg($"Load tilesheets.");
				var tilesheetLoader = CreateTilesheetLoader(args.Locations, args.InputPaths);
				var tilesheets = tilesheetLoader.Load();

				Output.Msg($"Join tiles.");
				var joinedTilesheet = Tilesheet.Join(tilesheets);

				Output.Msg($"Save tilesheet at: \"{args.OutputPath}\".");
				joinedTilesheet.Save(args.OutputPath);
			}
			catch(Exception ex)
			{
				Output.Error(ex.Message);
				Output.BlankLine();
				Output.Usage();
			}

			Output.BlankLine();
		}

		private static ITilesheetLoadStrategy CreateTilesheetLoader(Args.SourceLocations tilesheetLocation, string[] inputPaths)
		{
			if (tilesheetLocation == Args.SourceLocations.ByDirectory)
				return new ByDirTilesheetLoadStrategy(inputPaths[0]);
			else if (tilesheetLocation == Args.SourceLocations.ByFileName)
				return new ByFilePathTilesheetLoadStrategy(inputPaths);
			else
				throw new NotImplementedException();
		}
	}
}
