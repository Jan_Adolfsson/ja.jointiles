﻿using System;
using System.Linq;

namespace Ja.JoinTiles
{
	internal class Args
	{
		internal enum SourceLocations
		{
			ByFileName,
			ByDirectory
		}

		internal string[] InputPaths { get; }
		internal string OutputPath { get; }
		internal SourceLocations Locations {get;}

		internal static Args Parse(string[] argArray)
		{
			if (argArray.Length < 3)
				throw new Exception("Invalid number of arguments, must at least be 3.");

			var outputPath = argArray[0];
			var location = ParseLocation(argArray);

			var paths =
				argArray
					.Skip(2)
					.ToArray();

			var args = new Args(outputPath, location, paths);
			return args;
		}

		private static SourceLocations ParseLocation(string[] argArray)
		{
			var locationString = argArray[1];
			if (locationString == "-f")
			{
				return SourceLocations.ByFileName;
			}
			else if (locationString == "-d")
			{
				if (argArray.Length > 3)
					throw new Exception("Invalid number of arguments. Can only specify one path when using -d.");

				return SourceLocations.ByDirectory;
			}
			else
				throw new Exception("Second argument must be -f or -d.");
		}
		private Args(string outputPath, SourceLocations tilesheetLocation, string[] inputPaths)
		{
			OutputPath = outputPath;
			Locations = tilesheetLocation;
			InputPaths = inputPaths;
		}
	}
}