﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;

namespace Ja.JoinTiles
{
	internal interface ITilesheetLoadStrategy
	{
		IList<Tilesheet> Load();
	}

	class ByFilePathTilesheetLoadStrategy : ITilesheetLoadStrategy
	{
		readonly string[] _paths;

		internal ByFilePathTilesheetLoadStrategy(string[] paths)
		{
			_paths = paths;
		}
		public IList<Tilesheet> Load()
		{
			var images =
				_paths
					.Select(path =>
					{
						Output.Msg($"Load tilesheet at: \"{path}\".");

						var sheet = Tilesheet.Load(path);
						return sheet;						
					})
					.ToList();
			return images;
		}
	}

	class ByDirTilesheetLoadStrategy : ITilesheetLoadStrategy
	{
		readonly string _dir;

		internal ByDirTilesheetLoadStrategy(string dir)
		{
			_dir = dir;
		}
		public IList<Tilesheet> Load()
		{
			var filePaths = GetPngFilePaths();
			var images =
				filePaths
					.Select(path =>
					{
						Output.Msg($"Load tilesheet at: \"{path}\".");
						var sheet = Tilesheet.Load(path);
						return sheet;
					})
					.ToList();

			if (images.Count < 1)
				throw new Exception($"No .png files found at {_dir}.");
			return images;
		}

		private string[] GetPngFilePaths()
		{
			try
			{
				var filenames =
					Directory
						.EnumerateFiles(_dir, "*.png")
						.ToArray();

				return filenames;
			}
			catch (ArgumentException)
			{
				throw new Exception($"{_dir} contains invalid characters.");
			}
			catch (DirectoryNotFoundException)
			{
				throw new Exception($"Path {_dir} is invalid.");
			}
			catch (PathTooLongException)
			{
				throw new Exception($"Path {_dir} is too long.");
			}
			catch (IOException)
			{
				throw new Exception($"Path {_dir} is a filename.");
			}
			catch (SecurityException)
			{
				throw new Exception($"Permission denied to {_dir}.");
			}
			catch (UnauthorizedAccessException)
			{
				throw new Exception($"Permission denied to {_dir}.");
			}
		}
	}
}