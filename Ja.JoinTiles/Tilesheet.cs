﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;

namespace Ja.JoinTiles
{
	class Tilesheet
	{
		readonly Image _img;

		internal Size Size { get { return _img.Size; } }
		internal PixelFormat PixelFormat { get { return _img.PixelFormat; } }

		internal Tilesheet(Image img)
		{
			_img = img;
		}
		internal static Tilesheet Load(string inputPath)
		{
			try
			{
				var img = Image.FromFile(inputPath);
				var tilesheet = new Tilesheet(img);
				return tilesheet;
			}
			catch (OutOfMemoryException)
			{
				throw new Exception( $"File {inputPath} has invalid file format or pixel format not supported. Supported file types are: BMP, GIF, JPEG, PNG and TIFF. For alpha transparancy use PNG with 32 bits per pixel.");
			}
			catch (FileNotFoundException)
			{
				throw new Exception($"File {inputPath} not found.");
			}
			catch (ArgumentException)
			{
				throw new Exception($"Filename {inputPath} is a Uri.");
			}
		}
		internal void Save(string outputPath)
		{
			try
			{
				_img.Save(outputPath);
			}
			catch (ArgumentNullException)
			{
				throw new Exception("Failed saving new sheet. Filename is null.");
			}
			catch (ExternalException)
			{
				throw new Exception("Failed saving new sheet. Wrong format or trying to save to original file.");
			}
		}
		internal static Tilesheet Join(IList<Tilesheet> sheets)
		{
			var size = CalcSize(sheets);
			var destBmp = new Bitmap(size.Width, size.Height, PixelFormat.Format32bppArgb);
			using (var destFastBmp = new FastBitmap(destBmp))
			{
				for (int i = 0, y = 0; y < size.Height; y = y + sheets[i].Size.Height, ++i)
				{
					using (var srcBmp = new Bitmap(sheets[i]._img))
					{
						using (var srcFastBmp = new FastBitmap(srcBmp))
							destFastBmp.Insert(0, y, srcFastBmp);
					}
				}
			}

			var newTilesheet = new Tilesheet(destBmp);
			return newTilesheet;
		}

		private static Size CalcSize(IList<Tilesheet> sheets)
		{
			int width = 0, 
				height = 0;
			foreach(var sheet in sheets)
			{
				if (sheet.Size.Width > width)
					width = sheet.Size.Width;

				height += sheet.Size.Height;
			}

			var size = new Size(width, height);
			return size;
		}
	}
}
