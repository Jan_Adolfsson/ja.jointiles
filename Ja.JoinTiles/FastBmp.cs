﻿using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Ja.JoinTiles
{
	internal class FastBitmap : IDisposable
	{
		readonly Bitmap _bitmap;
		readonly BitmapData _data;
		readonly int _bytesPerPixel;
		readonly int _widthInBytes;

		internal FastBitmap(Bitmap bitmap)
		{
			_bitmap = bitmap;
			_data = _bitmap.LockBits(new Rectangle(Point.Empty, _bitmap.Size), ImageLockMode.ReadWrite, _bitmap.PixelFormat);

			_bytesPerPixel = Bitmap.GetPixelFormatSize(_bitmap.PixelFormat) / 8;
			_widthInBytes = _bitmap.Width * _bytesPerPixel;
		}
		public void Dispose()
		{
			_bitmap.UnlockBits(_data);
		}
		internal void Insert(int x, int y, FastBitmap src)
		{
			if (_bitmap.PixelFormat != src._bitmap.PixelFormat)
				throw new Exception("Different pixel format between source and destination image.");

			unsafe
			{
				byte*
					ptrFirstSrcPxl = (byte*)src._data.Scan0,
					ptrFirstDestPxl = (byte*)_data.Scan0;

				for (int srcY = 0, destY = y; srcY < src._bitmap.Height; ++srcY, ++destY)
				{
					byte* currSrcLine = ptrFirstSrcPxl + (srcY * src._data.Stride);
					byte* currDestLine = ptrFirstDestPxl + (destY * _data.Stride);

					for (int srcX = 0, destX = x * _bytesPerPixel; srcX < _bytesPerPixel * src._bitmap.Width; srcX = srcX + _bytesPerPixel, destX = destX + _bytesPerPixel)
					{
						int red = currSrcLine[srcX];
						int green = currSrcLine[srcX + 1];
						int blue = currSrcLine[srcX + 2];
						int alpha = currSrcLine[srcX + 3];

						currDestLine[destX] = (byte)red;
						currDestLine[destX + 1] = (byte)green;
						currDestLine[destX + 2] = (byte)blue;
						currDestLine[destX + 3] = (byte)alpha;
					}
				}
			}
		}
	}
}
