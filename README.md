# JILES

JILES (Join Tiles) is a command line tool for joining tilesheets.


### REMARKS  
A. Tech  
It's developed in C# using Visual Studio 2017 and for .NET Framework 4.6.2 but can easily be modified to run on earlier versions.  

The image processing is based on unsafe code so if you get in trouble working with the code you might have to open the Project Properties->Build and check Allow unsafe code.  

The image processing is based on numerous examples found on the net, just search for "fast image processing C#".  

B. Limitations  
The tool is only tested for 32 bpp argb PNGs.  


### GET STARTED  
Clone repo:  
```
git clone https://Jan_Adolfsson@bitbucket.org/Jan_Adolfsson/Ja.JoinTiles.git
```

Build:  
```
cd .\Ja.JoinTiles\Ja.JoinTiles\
dotnet build
```

Navigate to .exe:  
```
cd .\bin\debug
```


### USAGE  
jiles.exe destination [-f file0, ... , fileX | -d directory] 

_destination_  
Path to new tilesheet.  

-f
Join tilesheets specified by paths where file0, ..., fileX are any number of paths.

-d
Join tilesheets found in directory.

#### Join three tilesheets found at different locations
```
jiles.exe c:\game\assets\animations\hero.png -f c:\tiles\idle\hero_idle.png c:\tiles\attack\hero_attack.png c:\tiles\move\hero_move.png
```


#### Join all tilesheets found in a directory
```
jiles.exe c:\game\assets\animations\hero.png -d c:\tiles\hero
```
